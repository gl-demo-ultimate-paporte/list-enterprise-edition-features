# List Enterprise Edition Features

This script (`index.js`) gets all Enterprise features of GitLab.

## How to list features

```
node index.js > features.csv
```

## Contributions

Always welcome (issue/suggestions or MR).
An idea could be to auto-populate a google sheet and have scheduled runs.