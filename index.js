const node_dir = require('node-dir');
const fs = require('fs');
const { convertArrayToCSV } = require('convert-array-to-csv');


const premium_features = [];
const ultimate_features = [];
const features = [];
let fileRead = 0;

// this path needs to be the set to a local copy of https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc
node_dir.files('/Users/pap/development/gitlab/doc/', (err, files) => {
    const md_files = files
    .filter(file => file.split('.')[1] == 'md');
    md_files
    .map(file => {
        fs.readFile(file, (err, data) => {
            const fileContent = data.toString();
            let category = '';
            let stage, group = '';
            fileContent.split('\n').map(line => {
                if (line.startsWith('stage: ')) {
                    stage = line.split('stage: ')[1];
                } else if (line.startsWith('group: ')) {
                    group = line.split('group: ')[1];
                }

                category = line.startsWith('# ') ? line : category
                category = category.split('**')[0].split('#')[category.split('**')[0].split('#').length - 1 ].trim();

                if (line.startsWith('#') && line.includes('**')) {
                    let path = file.split('gitlab/doc/')[1];
                    path = path.replace('md', 'html');
                    let edition = 'community';

                    if (line.split('**')[1] == '(PREMIUM)') edition = 'premium';
                    if (line.split('**')[1] == '(ULTIMATE)') edition = 'ultimate';

                    let feature = line.split('**')[0].split('#')[line.split('**')[0].split('#').length -1].trim();

                    if (edition != 'community') {
                        features.push({
                            edition,
                            stage,
                            group,
                            feature: category != '' && category != feature ? category + ' >> ' + feature : feature,
                            link: `https://docs.gitlab.com/ee/${path + '#' + feature.toLowerCase().replaceAll(' ', '-').replaceAll(':', '').replaceAll('`', '')}`,
                        });
                    }
                }
            })
            fileRead++;
            if (fileRead == md_files.length) {
                const csvFromArrayOfObjects = convertArrayToCSV(features);
                console.log(csvFromArrayOfObjects);
            }
        });
    });
});

